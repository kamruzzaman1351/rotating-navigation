// UI Variables
const openBtnUI = document.getElementById("open");
const closeBtnUI = document.getElementById("close");
const mainDivUI = document.querySelector(".container");


// Event Listner
openBtnUI.addEventListener("click", () => {
    mainDivUI.classList.add("show-nav");
});
closeBtnUI.addEventListener("click", () => {
    mainDivUI.classList.remove("show-nav")
});